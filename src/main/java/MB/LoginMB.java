/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MB;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import org.springframework.stereotype.Component;

/**
 *
 * @author leola
 */
@Component
@SessionScoped
public class LoginMB implements Serializable{
    
    private String nome, senha, msgErro;
    
    public LoginMB() {
        msgErro="";
    }
    
    public String verificaPassword(){
        if(nome.equals("admin")&& senha.equals("123")){
            
            return "index";
        }else{
            msgErro="login ou senha incorreta";
            return"";
        }
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getMsgErro() {
        return msgErro;
    }

    public void setMsgErro(String msgErro) {
        this.msgErro = msgErro;
    }
    
    
}
